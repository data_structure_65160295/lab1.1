/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab1_1;

/**
 *
 * @author natta
 */
public class Lab1_1 {

   public static void main(String[] args) {
        int[] numbers = {5, 8, 3, 2, 7};
        String[] names = {"Alice", "Bob", "Charlie", "David"};
        double[] values = new double[4];

        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i] + " ");
        }

        System.out.println();

        for (String c : names) {
            System.out.print(c + " ");

        }

        System.out.println();
        values[0] = 5.55;
        values[1] = 3.26;
        values[2] = 16.54;
        values[3] = 27.89;

        int sum = 0;
        for (int number : numbers) {
            sum = sum + number;
        }
        System.out.println(sum);

        double maxV = values[0];
        for (double V : values) {
            if (V > maxV) {
                maxV = V;
            }
        }
        System.out.println(maxV);
        
        String[] reversedNames = new String[names.length];

        for (int i =0; i < names.length; i++){
            reversedNames[names.length - 1 - i] = names[i];
        }
        
        for (String k : reversedNames) {
            System.out.print(k + " ");
        }
        
        for (int i = 0; i < numbers.length - 1; i++) {
            for (int j = 0; j < numbers.length - i - 1; j++) {
                if (numbers[j] > numbers[j + 1]) {
                    int temp = numbers[j];
                    numbers[j] = numbers[j + 1];
                    numbers[j + 1] = temp;
                }
            }
        }
        
        System.out.println();
        for (int number : numbers) {
            System.out.print(number+" ");
        }
    }
}
